﻿namespace TapeFM.Server.Models
{
    public class DirectoryEntry
    {
        public string FullPath { get; set; }
        public string Name { get; set; }
        public bool IsDirectory { get; set; }
    }
}