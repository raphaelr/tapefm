﻿namespace TapeFM.Server.Models
{
    public class RadioStationConfiguration
    {
        public int BitrateKbps { get; set; }
        public EmptyPlaylistMode EmptyPlaylistMode { get; set; }
    }
}