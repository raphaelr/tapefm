﻿namespace TapeFM.Server.Models
{
    public enum EmptyPlaylistMode
    {
        RandomMix,
        Silence,
        RepeatLast
    }
}