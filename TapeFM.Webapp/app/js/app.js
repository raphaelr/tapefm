﻿Ext.application({
    name: "TapeFM",
    autoCreateViewport: "TapeFM.view.Main",

    init: function() {
        Ext.setGlyphFontFamily("FontAwesome");
    }
});
